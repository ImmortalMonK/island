﻿using UnityEngine;
using System.Collections;

public class Fall : MonoBehaviour {
	public float spawnX,spawnY, spawnZ;
	// Use this for initialization
	void Start () {
		spawnX = transform.position.x;
		spawnY = transform.position.y;
		spawnZ = transform.position.z;
	}
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.name == "Lava") {

		transform.position = new Vector3(spawnX, spawnY,spawnZ);
	}
	
}
}
