﻿using UnityEngine;
using System.Collections;

public class sound : MonoBehaviour {

	//public AudioClip myClip;

	// Use this for initialization
	void Start () {
	
		//audio.PlayOneShot (myClip);// 1 вариант
		audio.Play ();// 2 вариат в скобках в кадрах прописывается задержка(1 секунда = 44100 кадров)
	}
	
	// Update is called once per frame
	void Update () {
	
		if (!audio.isPlaying) {
			audio.Play();
		}
	}
}
